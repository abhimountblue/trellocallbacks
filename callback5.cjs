const findBoardsById = require('./callback1.cjs')
const findAllListOfBoardId = require('./callback2.cjs')
const findAllCardOfListId = require('./callback3.cjs')

function problem5() {
    findBoardsById('mcu453ed', (err, boardData) => {
        if (err) {
            console.error(err)
        } else {
            console.log(boardData)
            const thanosID = boardData.id
            findAllListOfBoardId(thanosID, (err, listData) => {
                if (err) {
                    console.error(err)
                } else {
                    console.log(listData)
                    const findMind = listData.filter((element) => {
                        if (element.name === 'Mind' || element.name === 'Space') {
                            return element
                        }
                    })
                    if (findMind.length === 0) {
                        console.error('list not found with the name')
                    } else {
                        printCards(findMind, 0)
                    }
                }
            })
        }
    })
}

function printCards(listIds, number = 0) {
    if (listIds.length > number) {
        findAllCardOfListId(listIds[number].id, (err, cardData) => {
            if (err) {
                console.error(err)
            } else {
                console.log(cardData)
            }
        })
        printCards(listIds, number + 1)
    }
    return
}
module.exports = problem5