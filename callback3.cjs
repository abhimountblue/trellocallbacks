/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs')
function problem3(listId, callback) {
    setTimeout(() => {
        fs.readFile('cards.json', 'utf-8', (err, data) => {
            if (err) {
                callback(err)
            } else {
                const cards = JSON.parse(data)
                if (cards[listId]) {
                    callback(null, cards[listId])
                } else {
                    callback('card not found by given list id')
                }
            }
        })
    }, 2 * 1000)
}

module.exports = problem3