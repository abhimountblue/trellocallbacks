const findBoardsById = require('./callback1.cjs')
const findAllListOfBoardId = require('./callback2.cjs')
const findAllCardOfListId = require('./callback3.cjs')

function problem4() {
    findBoardsById('mcu453ed', (err, boardData) => {
        if (err) {
            console.error(err)
        } else {
            console.log(boardData)
            const thanosID = boardData.id
            findAllListOfBoardId(thanosID, (err, listData) => {
                if (err) {
                    console.error(err)
                } else {
                    console.log(listData)
                    const findMind = listData.find((element) => {
                        if (element.name === 'Mind') {
                            return element
                        }
                    })
                    if (typeof findMind === 'undefined') {
                        console.error('list not found with the name of Mind')
                    } else {
                        findAllCardOfListId(findMind.id, (err, cardData) => {
                            if (err) {
                                console.error(err)
                            } else {
                                console.log(cardData)
                            }
                        })
                    }
                }
            })
        }
    })
}
module.exports = problem4