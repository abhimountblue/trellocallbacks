const fs = require('fs')
function problem1(id, callback) {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) {
                callback(err)
            } else {
                const boards = JSON.parse(data)
                const board = boards.find((element) => {
                    return element.id === id
                })
                if (typeof board === 'undefined') {
                    callback('board is not found by given id')
                } else {
                    callback(null, board)
                }
            }
        })

    }, 2 * 1000)
}

module.exports = problem1
