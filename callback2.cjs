/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs')
function problem2(boardId, callback) {
    setTimeout(() => {
        fs.readFile('lists.json', 'utf-8', (err, data) => {
            if (err) {
                callback(err)
            } else {
                const lists = JSON.parse(data)
                if (lists[boardId]) {
                    callback(null, lists[boardId])
                } else {
                    callback('list not found by given board Id')
                }
            }
        })

    }, 2 * 1000)
}

module.exports = problem2